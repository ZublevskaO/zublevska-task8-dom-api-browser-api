window.onload = () => {

    fetch('https://rickandmortyapi.com/api/character')
      .then(response => response.json())
      .then(data => {
                const rawData = data.results;
                return rawData.map(character => {
                  //all needed data is listed below as an entity 
                   let created = new Date(character.created).toLocaleString();
                //    let date = created.getDate();
                   let species = character.species ;
                   let img = character.image;
                   let episodes = character.episode;
                   let name = character.name;
                   let location = character.location.name;
                    //create element
                    let list = document.querySelector('.container');
                    newDiv = document.createElement('DIV');
                    newDiv.classList.add('card');
                    newDiv.innerHTML = `<ul><li><h3> ${name}</h3></li>
                    <li><img src="${img}" class="card-image"></li>
                    <li>Species: ${species}</li>
                    <li>Location: ${location}</li>
                    <li>Created: ${created}</li>
                    <li>Episodes: <a href="${episodes}">link</a></li></ul>`;
                    //append element
                    list.appendChild(newDiv);
                });
            })
    }

    window.addEventListener('scroll', function() {
        document.getElementById('showScroll').innerHTML = pageYOffset + 'px';
      });

    let filter = function () {
        let input = document.getElementById('site-search');
        input.addEventListener('input', function () {
            let filter = input.value.toLowerCase(),
            filterItems = document.querySelectorAll('name');
            filterItems.forEach(item => {
                if (item.innerHTML.toLowerCase().indexOf(filter) > -1) {
                    item.style.display = '';
               } else {
                   item.style.display = 'none';
               }
                       })
                   })
    };